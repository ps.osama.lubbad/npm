1- install npm:
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
sudo apt-get install -y nodejs

2- clone the repo.
git clone git@gitlab.com:ps.osama.lubbad/npm.git

3- clone the devops-practices sample jar and follow readme.md to compile and start the jar file.
git@gitlab.com:ps.osama.lubbad/devops-practices.git

4- to send and recieve requests with the sample jar run the following.
to test connection with the jar file.
- npm run test.

to create a new person.
- npm run create.

to list persons.
- npm run list.

to edit person's info.
- npm run edit.

to get person's info.
- npm run get.

to delete person data.
- npm run delete.